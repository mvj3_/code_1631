//
//  main.m
//  Learning0506

//  判断整数是否相等

//  Created by  apple on 13-5-6.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>

//判断整数是否相等，返回BOOL
BOOL areIntsDifferent(int number1, int number2){
    
    return number1 != number2;
   
}

BOOL areIntsDifferent_fault(int number1 , int number2){
    return number1 - number2;
}

//返回BOOL对应的文字，if的缺省判断
NSString * boolstring(BOOL yesNo){
    if(yesNo){  //不要直接跟YES比较，因为在oc中 1不等于YES， 另外也可以直接写if(yesNo)
        return @"YES";
    }else{
        return @"NO";
    }
}

//返回BOOL对应的文字， BOOL与NO比较
NSString * boolstring_NO(BOOL yesNo){
    if(yesNo == NO){  //不要直接跟YES比较，因为在oc中 1不等于YES， 另外也可以直接写if(yesNo)
        return @"NO";
    }else{
        return @"YES";
    }
}

//返回BOOL对应的文字，BOOL与YES比较
NSString * boolstring_YES(BOOL yesNo){
    if(yesNo == YES){  //不要直接跟YES比较，因为在oc中 1不等于YES， 另外也可以直接写if(yesNo)
        return @"NO";
    }else{
        return @"YES";
    }
}

int main(int argc, const char * argv[])
{
    NSLog(@"Hello, World!");
    //if的缺省比较，正确的
    NSLog(boolstring(areIntsDifferent(23, 23)));
    
    //BOOL值与NO比较，正确的
    NSLog(boolstring_NO(areIntsDifferent_fault(23, 23)));
    
    //BOOL值与YES比较,错误的
    NSLog(boolstring_YES(areIntsDifferent_fault(23, 23)));
    return 0;
}
